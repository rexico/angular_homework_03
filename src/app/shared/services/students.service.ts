import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { LocalStorageService } from "src/app/shared/services/local-storage.service";
import { environment } from "src/environments/environment";
import { Student } from "../../modules/journal/components/student/interfaces/student.interface";

@Injectable()
export class StudentsService {
    students!: Student[];

    constructor(private localStorageService: LocalStorageService) {
        this.getAllStudents();
        if (this.students == null) {this.init();}
        
    }
    getAllStudents(): Observable<Student[]>{
        this.students = JSON.parse(String(this.localStorageService.get(environment.STUDENTS)));
        return of(this.students);
    }

    addStudent(student: Student) {
        console.log(student);
        this.students.push(student);
        this.localStorageService.set(environment.STUDENTS, JSON.stringify(this.students));
    }

    getId(): number { 
        let maxId = 0;
        for (var s of this.students) {
            console.log(s); 
            maxId = Math.max(maxId, s.id);
          }
        return ++maxId; 
    }

    getStudent(id: number): Student | undefined{ 
      return this.students.find(s => s.id == id);
    }

    editStudent(student: Student) {
        this.students[this.students.findIndex(s => s.id == student.id)] = student;
        this.localStorageService.set(environment.STUDENTS, JSON.stringify(this.students));
    }

    deleteStudent(id: number) {
        const index: number = this.students.findIndex(s => s.id == id);
        if (index != -1) {
            this.students.splice(index, 1);
            this.localStorageService.set(environment.STUDENTS, JSON.stringify(this.students));
        }     
    }

    init() {
        this.students = [
            {
                id: 1,
                firstName: 'firstName1',
                lastName: 'lastName1',
                group: 'group1',
                age: 25
            },
            {
                id: 2,
                firstName: 'firstName2',
                lastName: 'lastName2',
                group: 'group1',
                age: 23
            },
            {
                id: 3,
                firstName: 'firstName3',
                lastName: 'lastName3',
                group: 'group2',
                age: 21
            },
        ];
        this.localStorageService.set(environment.STUDENTS, JSON.stringify(this.students));

    }
}

