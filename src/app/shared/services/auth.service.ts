import {Injectable} from "@angular/core";
import { environment } from "src/environments/environment";
import {LocalStorageService} from "./local-storage.service";

@Injectable()
export class AuthService {
    isLoggedIn = false;
  constructor(private localStorageService: LocalStorageService) {
  }
  isAuth(): boolean {
        return this.localStorageService.get(environment.ISAUTH) != null ? true : false;
  }
}
