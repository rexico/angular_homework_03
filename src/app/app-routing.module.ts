import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './components/error/error.component';
import { AddStudentComponent } from './modules/add/add.component';
import { EditStudentComponent } from './modules/edit/edit.component';
import { JournalComponent } from './modules/journal/journal.component';
import { LoginComponent } from './modules/login/login.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  {
    path: '', component: LoginComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'journal', component: JournalComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add/student', component: AddStudentComponent, canActivate: [AuthGuard]
  },
  {
    path: 'journal/student/:id', component: EditStudentComponent, canActivate: [AuthGuard]
  },
  {
    path: '**', component: ErrorComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
