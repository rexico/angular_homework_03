import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentsService } from 'src/app/shared/services/students.service';
import { Student } from '../journal/components/student/interfaces/student.interface';

@Component({
  selector: 'edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditStudentComponent implements OnInit{
    editForm!: FormGroup;
    student!: Student;
    constructor(private readonly studentsService: StudentsService, private activatedRoute: ActivatedRoute, private router: Router) {
        this.activatedRoute.params.subscribe(p => {
            let s = studentsService.getStudent(p.id);
            if ( s ) {
                this.student = s;
            }
        });
    }

    ngOnInit(): void {
        this.editForm = new FormGroup(
        {
            "firstName": new FormControl(this.student.firstName, Validators.required),
            "lastName": new FormControl(this.student.lastName, Validators.required),
            "group": new FormControl(this.student.group, Validators.required),
            "age": new FormControl(this.student.age, [Validators.required, Validators.min(15), Validators.max(60),])
        }
        );

    }

    onBtnSaveClick(): void  {
        if (this.editForm.valid) {

            this.student.firstName = this.editForm.get('lastName')?.value,
            this.student.lastName = this.editForm.get('firstName')?.value,
            this.student.group = this.editForm.get('group')?.value,
            this.student.age =  this.editForm.get('age')?.value
            
            this.studentsService.editStudent(this.student)
            this.router.navigate(['/journal']);
        }
    }

    onBtnDeleteClick(): void  {
        this.studentsService.deleteStudent(this.student.id)
        this.router.navigate(['/journal']);
   }
}