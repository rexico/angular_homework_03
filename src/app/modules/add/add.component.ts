import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StudentsService } from 'src/app/shared/services/students.service';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddStudentComponent implements OnInit{
    addForm!: FormGroup;

    constructor(private readonly studentsService: StudentsService) {
        
    }

    ngOnInit(): void {
        this.addForm = new FormGroup(
        {
            "firstName": new FormControl('', Validators.required),
            "lastName": new FormControl('', Validators.required),
            "group": new FormControl('', Validators.required),
            "age": new FormControl(0, [Validators.required, Validators.min(15), Validators.max(60),])
        }
        );
    }

    onBtnClick(): void  {
        if (this.addForm.valid) {
            this.studentsService.addStudent(
                {
                    id: this.studentsService.getId(),
                    firstName: this.addForm.get('firstName')?.value,
                    lastName: this.addForm.get('lastName')?.value,
                    group: this.addForm.get('group')?.value,
                    age: this.addForm.get('age')?.value
                }
            );
            this.addForm.reset();
        }
    }
}