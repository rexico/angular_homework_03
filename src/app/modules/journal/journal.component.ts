import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentsService } from '../../shared/services/students.service';
import {delay} from 'rxjs/operators'
import { Student } from './components/student/interfaces/student.interface';

@Component({
  selector: 'journal',
  templateUrl: './journal.component.html',
  styleUrls: ['./journal.component.css']
})
export class JournalComponent implements OnInit{

    students$!: Observable<Student[]>;

    constructor (private readonly studentService: StudentsService) {
        // console.log("--studentService.getAllStudents constructor");
        // this.students$ = this.studentService.getAllStudents().pipe(delay(1000));
    }

    ngOnInit(): void {
        this.students$ = this.studentService.getAllStudents().pipe(delay(1000));
    }

}
