import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { StudentComponent } from './components/student/student.component';
import { StudentsService } from '../../shared/services/students.service';
import { JournalComponent } from './journal.component';

@NgModule({
  declarations: [
    JournalComponent,
    StudentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    BrowserAnimationsModule
  ],
  providers: [StudentsService, LocalStorageService],

})
export class JournalModule { }
