export interface Student {
    id: number;
    firstName: string;
    lastName: string;
    group: string;
    age: number;
}