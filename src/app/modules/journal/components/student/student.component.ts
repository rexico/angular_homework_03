import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { Student } from "./interfaces/student.interface";



@Component({
    selector: 'student',
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.css']
  })
  export class StudentComponent {
    @Input() student!: Student;

    constructor(private router: Router) {
    }

    onStudentClick() {
      this.router.navigate(['journal/student', this.student.id]);
    }
  
  }
