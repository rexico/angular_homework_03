import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  loginForm!: FormGroup;

  constructor(private readonly localStorageService: LocalStorageService, private router: Router) {
    
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup(
      {
        "login": new FormControl('', [Validators.required, Validators.email]),
      }
    );
  }

  onBtnClick(): void  {
    if (this.loginForm.valid) {
      this.localStorageService.set(environment.ISAUTH, this.loginForm.get("login")?.value);
      this.loginForm.reset();
      this.router.navigate(['/journal']);
    }
  }
}