import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import { LoginModule } from './modules/login/login.module';
import { ErrorComponent } from './components/error/error.component';
import { JournalModule } from './modules/journal/journal.module';
import { AuthGuard } from './shared/guards/auth.guard';
import { AuthService } from './shared/services/auth.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AddStudentModule } from './modules/add/add.module';
import { EditStudentModule } from './modules/edit/edit.module';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatListModule,
    LoginModule,
    JournalModule,
    MatFormFieldModule,
    AddStudentModule,
    EditStudentModule
  ],
  providers: [AuthGuard, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
