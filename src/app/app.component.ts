import { Component } from '@angular/core';
import { Observer } from 'rxjs';
import { Observable } from 'rxjs';



export interface Tab {
  label: string;
  content: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  asyncTabs: Observable<Tab[]>;
  
  title = 'homework03';

  constructor() {
    console.log("--AppComponent constructor");
    this.asyncTabs = new Observable((observer: Observer<Tab[]>) => {
      setTimeout(() => {
        observer.next([
          {label: 'Login', content: '/login'},
          {label: 'Journal', content: '/journal'},
          {label: 'Add Student', content: '/add/student'},
        ]);
      }, 1000);
    });
    
  }
}
